#include <stdio.h>
#include "opencv2/opencv.hpp"
#include <opencv2/imgproc/imgproc.hpp>

#define errorCheck(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
	if (code != cudaSuccess) {
		fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) exit(code);
	}
}

using namespace cv;

__global__ void threshold(unsigned char *input, unsigned char *output)
{
	int x = blockIdx.x * blockDim.x + threadIdx.x;
	int y = blockIdx.y * blockDim.y + threadIdx.y;
	int idx = y*(blockDim.x*gridDim.x) + x;

	if( input[idx] < 120 ){
		output[idx] = 0;
	} else {
		output[idx]  = 255;
	}
}

int main()
{
	VideoCapture cap(0); // open the default camera
	if(!cap.isOpened())  // check if we succeeded
		return -1;
	
	// setup window for result display
	namedWindow( "SourceImg", WINDOW_AUTOSIZE );// Create a window for display.
	namedWindow( "ResultImg", WINDOW_AUTOSIZE );// Create a window for display.

	unsigned char *h_output;
	unsigned char *d_input, *d_output;

	// grab test frame and convert to grayscale
	Mat frame, frame_tmp;
	cap >> frame_tmp;
	cv::cvtColor(frame_tmp, frame, cv::COLOR_BGR2GRAY);	

	int width  = frame.rows;
	int height = frame.cols;
	int imgSize = width*height*sizeof(unsigned char);

	// create blocks and threads
	// 	.. can be tuned for specific device hardware
	dim3 threadsPerBlock(16, 16);
	dim3 numBlocks(width / threadsPerBlock.x, height / threadsPerBlock.y);

	// malloc pinned memory on host to store result image 
	cudaMallocHost(&h_output, imgSize);
	
	// malloc space on device to store source & result images
	cudaMalloc(&d_input,  imgSize);
	cudaMalloc(&d_output, imgSize);

	for(;;) {		            
		cap >> frame_tmp; // get a new frame from camera
		cv::cvtColor(frame_tmp, frame, cv::COLOR_BGR2GRAY);	

		cudaMemcpy(d_input, frame.data, imgSize, cudaMemcpyHostToDevice);
		threshold<<<numBlocks, threadsPerBlock>>>(d_input, d_output);
		errorCheck(cudaGetLastError());
		cudaMemcpy(h_output, d_output, imgSize, cudaMemcpyDeviceToHost);

		frame = Mat(width, height, CV_8UC1, h_output, Mat::AUTO_STEP);

		imshow( "ResultImg", frame     );   // Show our image inside it.
		imshow( "SourceImg", frame_tmp );   // Show our image inside it.
		
		waitKey(1);
	}

	return 0;
}
